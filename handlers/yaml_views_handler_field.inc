<?php

class yaml_views_handler_field extends views_handler_field {
  function render($values) {
    return check_plain($values[$this->field_alias]);
  }


  function option_definition() {
    $options = parent::option_definition();
    $options['column_name'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['column_name'] = array(
      '#title' => t('Column name'),
      '#description' => t('The name of the column resp. attribute'),
      '#type' => 'textfield',
      '#default_value' => $this->options['column_name'],
      '#required' => TRUE,
    );
  }

  /**
   * Called to add the field to a query.
   */
  function query() {
    // Add the field.
    $this->field_alias = $this->query->add_field($this->table_alias, $this->options['column_name']);
  }

  /**
   * Provide extra data to the administration form
   */
  function admin_summary() {
    return $this->label();
  }
}
