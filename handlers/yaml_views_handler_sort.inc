<?php

class yaml_views_handler_sort extends views_handler_sort {
  function option_definition() {
    $options = parent::option_definition();
    $options['column_name'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['column_name'] = array(
      '#title' => t('Column name'),
      '#description' => t('The column name'),
      '#type' => 'textfield',
      '#default_value' => $this->options['column_name'],
      '#required' => TRUE,
    );
  }

  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->query->add_orderby($this->table_alias, $this->options['column_name'], $this->options['order']);
  }
}
