<?php

/**
 * Implements hook_views_data
 */

function yaml_views_views_data() {
  $data['yaml']['table']['group'] = t('YAML');

  $data['yaml']['table']['base'] = array(
    'title' => t('YAML'),
    'help' => t('Queries an YAML file.'),
    'query class' => 'yaml_views',
  );

  $data['yaml']['column'] = array(
    'title' => t('Column'),
    'help' => t('An column in the YAML file.'),
    'field' => array(
      'handler' => 'yaml_views_handler_field',
      'click sortable' => TRUE,
    ),
      /*
    'filter' => array(
      'handler' => 'yaml_views_handler_filter',
    ),
       * 
       */
    'sort' => array(
      'handler' => 'yaml_views_handler_sort',
    ),
/*
      'argument' => array(
      'handler' => 'yaml_views_handler_argument_column',
    )
  */
  );
 
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function yaml_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'yaml_views') . '/handlers',
    ),
    'handlers' => array(
      // Fields
      'yaml_views_handler_field' => array(
        'parent' => 'views_handler_field',
      ),
      // Filters
      'yaml_views_handler_filter' => array(
        'parent' => 'views_handler_filter',
      ),
      // Sort handlers
      'yaml_views_handler_sort' => array(
        'parent' => 'views_handler_sort',
      ),
/*
      // Argument handlers
      'yaml_views_handler_argument' => array(
        'parent' => 'views_handler_argument',
      ),
*/
    ),
  );
}

/**
 * Implementation of hook_views_plugins().
 */
function yaml_views_views_plugins() {
  return array(
    'query' => array(
      'yaml_views' => array(
        'title' => t('YAML'),
        'help' => t('Reads from an YAML file.'),
        'handler' => 'yaml_views_plugin_query_yaml',
      ),
    ),
  );
}
