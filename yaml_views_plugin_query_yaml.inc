<?php

class yaml_views_plugin_query_yaml extends views_plugin_query {

  /**
   * Constructor; Create the basic query object and fill with default values.
   */
  function init($base_table = '', $base_field, $options = array()) {
    parent::init($base_table, $base_field, $options);

    $this->yaml_file = $this->options['yaml_file'];
  }

  function use_pager() {
    return FALSE;
  }

  /**
   * Generate a query and a countquery from all of the information supplied
   * to the object.
   *
   * @param $get_count
   *   Provide a countquery if this is true, otherwise provide a normal query.
   */
  function query($view, $get_count = FALSE) {
    return '';
   }


  function add_param($param, $value = '') {
    $this->params[$param] = $value;
  }

  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    $view->build_info['query'] = $this->query($view);
    $view->build_info['count_query'] = '';
    $view->build_info['query_args'] = array();
  }

  function execute(&$view) {
    $start = views_microtime();
    $yaml = file_get_contents($this->options['yaml_file']);
    $data = yaml_parse($yaml);
 
    $result = array();

//    $group = $data['win'];
    foreach ($data as $group) {
    //  dsm(var_dump($group));
        
      if (array_key_exists('labor', $group)) continue;

      foreach ($group as $row) {
        $item = array();
        foreach ($this->fields as $field) {
          $field_id = $field['field'];
          if (array_key_exists($field_id, $row)) {
            $item[$field_id] = $row[$field['field']];
          } else {
            $item[$field_id] = 'n/a';
          }
        }
        if (!empty($this->orderby)) {
          foreach ($this->orderby as $orderby) {
            if (array_key_exists($orderby['field'], $row)) {
              $item[$orderby['field']] = $row[$field['field']];
            }
          }
        }
        $result[] = $item;
      }
      if (!empty($this->orderby)) {
        // Array reverse, because the most specific are first - PHP works the
        // opposite way of SQL.
        foreach (array_reverse($this->orderby) as $orderby) {
          _yaml_views_sort_field($orderby['field'], $orderby['order']);
          uasort($result, '_yaml_views_sort');
        }
      }
    }

    $view->result = $result;
    $view->total_rows = count($result);

    $view->execute_time = views_microtime() - $start;
  }

  function add_signature(&$view) {}

  function option_definition() {
    $options = parent::option_definition();
    $options['yaml_file'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['yaml_file'] = array(
      '#type' => 'textfield',
      '#title' => t('YAML File'),
      '#default_value' => $this->options['yaml_file'],
      '#description' => t("The URL or path to the YAML file."),
    );
  }



  function add_field($table, $field, $alias = '', $params = array()) {
    $alias = $field;

    // Create a field info array.
    $field_info = array(
      'field' => $field,
      'table' => $table,
      'alias' => $field,
    ) + $params;

    if (empty($this->fields[$field])) {
      $this->fields[$field] = $field_info;
    }

    return $field;
  }

  function add_orderby($table, $field, $order, $alias = '', $params = array()) {
    $this->orderby[] = array(
      'field' => $field,
      'order' => $order,
    );
  }
  
/*
  function add_filter($group, $operator, $xpath_selector, $value) {
    $this->filter[] = array(
      'group' => $field,
      'operator' => $operator,
      'xpath_selector' => $xpath_selector,
      'value' => $value,
    );
  }
}
*/
}

function _yaml_views_sort_field($field = NULL, $direction = NULL) {
  static $f;
  if ($field) {
    $f = array('field' => $field, 'direction' => $direction);
  }
  return $f;
}

function _yaml_views_sort($a, $b) {
  $sort = _yaml_views_sort_field();
  $field = $sort['field'];
  $a_weight = (is_array($a) && isset($a[$field])) ? $a[$field] : 0;
  $b_weight = (is_array($b) && isset($b[$field])) ? $b[$field] : 0;
  if ($a_weight == $b_weight) {
    return 0;
  }
  if (strtolower($sort['direction']) == 'asc') {
    return ($a_weight < $b_weight) ? -1 : 1;
  }
  else {
    return ($a_weight > $b_weight) ? -1 : 1;
  }
}

